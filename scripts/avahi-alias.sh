#!/bin/bash

ip_addr=$(avahi-resolve -4 -n $(hostname).local | cut -f 2)
alias_name="${1}.local"

if [ $ip_addr != "127.0.0.1" ]; then
    echo "Aliasing ${ip_addr} as ${alias_name}"
    avahi-publish -a -R ${alias_name} ${ip_addr}
else
    echo "Exiting, local address is $local."
    exit 1
fi
